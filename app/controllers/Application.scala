package controllers

import models.TestActor.FindAll
import play.api._
import play.api.libs.Comet
import play.api.mvc._
import scala.collection._
import play.twirl.api.Html
import play.api.libs.iteratee._
import models._
import play.api.libs.json.{ Json, Format }
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import akka.util.Timeout
import scala.concurrent.Await
import scala.concurrent.duration._

import play.api.cache._
import javax.inject.Inject
import scala.language.postfixOps._

import akka.actor._
import akka.pattern.ask

import scala.concurrent.{Future, future}
import scala.util.{Failure, Success}

import play.api.libs.iteratee.Enumerator

class Application @Inject() (cache: CacheApi) extends Controller {
    implicit val  testFormat=Json.format[TEST]
  def index = Action{ implicit  request =>
    val s = """{"name":"jiang","age":19}"""
//    render{
//      case Accepts.Json() => Ok(Json.toJson(s))
//      case Accepts.Html() => Ok(views.html.index(s))
//    }

//    val file = new java.io.File("/home/jiang/桌面/whipper_project.pdf")
//    val fileContent: Enumerator[Array[Byte]] = Enumerator.fromFile(file)
//    import scala.collection.immutable._
//    Result(
//      header = ResponseHeader(200, Map(CONTENT_LENGTH -> file.length.toString)),
//      body = fileContent
//    )
      // 下载文件 or 在线阅读
//    Ok.sendFile(
//      content = new java.io.File("/tmp/fileToServe.pdf"),
//      inline = true
//    )

//    Result(
//      header = ResponseHeader(200),
//      body = Enumerator("Hello World".toArray[Byte])
//    )



//    Logger.info("Start")
//    //  val data=eActor ? Find(1)
//    // val s=Await.result(data,timeout.duration)
//    // println(s)
    Ok(views.html.index("Your new application is ready."))
  }
    def findasync(id:Long)=Action.async {

      TESTCon.getByID(id).map(user=>Ok(Json.toJson(user)))
    }
    def insert=Action.async{implicit request =>
      val rec = request.body.asJson.get
      val username = (rec \ "name").asOpt[String]
      val testuser = TEST(username)
        TESTCon.insert(testuser).map(p=>Ok(Json.toJson(p)))
    }
    def delete(id:Long)=Action.async{implicit request=>
        TESTCon.delete(id).map(p=>Ok(Json.toJson(p)))
    }
    def findAll=Action.async{ implicit  request=>

      import java.util.concurrent.TimeoutException


       val data=TESTCon.findAll()
        data.map(p=>Ok(Json.toJson(p))).recover{
          case ex: TimeoutException =>
            Logger.error("Problem found in employee list process")
            InternalServerError(ex.getMessage)
        }
    }

//  import connect with Akka
    import models.TestActor.{Find,Insert,Delete,FindAll}

    implicit val timeout = Timeout(5 seconds)
    implicit lazy val system=ActorSystem()
    implicit lazy val actor=system.actorOf(Props(new TestActor()))
    def find_akka(id:Long)=Action{
        val future=actor ? Find(id)


        val ret=Await.result(future,timeout.duration).asInstanceOf[Option[TEST]]
        Ok(Json.toJson(ret))
    }
    def insert_akka()=Action{
        val testuser=TEST(Option("jiang"))
        val future=actor ? Insert(testuser)
        val ret =Await.result(future,timeout.duration).asInstanceOf[TEST]
        Ok(Json.toJson(ret))
    }
    def delete_akka(id:Long)=Action{
        val future=actor ? Delete(id)
        val ret =Await.result(future,timeout.duration).asInstanceOf[Option[TEST]]
        Ok(Json.toJson(ret))
    }
    def findall_akka()=Action{
        val future=actor ? FindAll()
        val ret =Await.result(future,timeout.duration).asInstanceOf[Vector[TEST]]
        Ok(Json.toJson(ret))
    }

//  def comet = Action {
//    val steam = TESTCon.stream()
//
//
//
//    val events = Enumerator(
//
//      """{"name":"jiang","age":20}"""
//    )
//
////    val events = Enumerator("kiki", "foo", "bar")
//
//
//    Ok.chunked(events).as(JSON)
//  }

  def comet = WebSocket.using[String] { request =>

    // Just consume and ignore the input
    val in = Iteratee.consume[String]()

    // Send a single 'Hello!' message and close
    val out = Enumerator("Hello!") >>> Enumerator.eof

    (in, out)
  }










}


