package globals
import play.api._
import play.api.Play.current
import play.filters.gzip.GzipFilter
import play.api.db._
import play.api.Play.current
import slick.driver.MySQLDriver.api._
import play.api.mvc._
import play.api.mvc.{Handler, RequestHeader}






object Global extends  WithFilters(new GzipFilter()) with GlobalSettings {

     def db = Database.forDataSource(DB.getDataSource("default"))   //
    override def onStart(app: Application) {
        Logger.info("Application has started.")
    }
    override def onStop(app: Application) {
        Logger.info("Application has stopped.")
        }

    
}