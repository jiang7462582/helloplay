package models


import play.api._
import play.api.mvc._
import play.api.Play.current
import scala.concurrent._

/**
 * Created by jiang on 15-9-9.
 */
object LoggingAction extends ActionBuilder[Request] {
  def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) = {
    Logger.info("Calling action")
    Logger.info("maybe you want to do more things here")
    //do more thing
    block(request)
  }
}