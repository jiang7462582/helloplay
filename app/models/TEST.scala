package models

import play.api.libs.json.{ Json, Format }
import slick.driver.MySQLDriver.api._
import play.api.libs.concurrent.Execution.Implicits._
import slick.jdbc.GetResult
import scala.concurrent.Future
import play.api.Play.current
import globals._
import akka.actor.Actor
import akka.pattern._

import scala.concurrent.stm.Sink
import scala.util.{Failure, Success}

case class TEST(
    var name:Option[String]=None,
    var id:Option[Long]=None
)

class TestTable(tag:Tag) extends Table[TEST](tag,"TEST"){
    def id=column[Option[Long]]("ID",O.PrimaryKey,O.AutoInc)
    def name=column[Option[String]]("name")
    def * =(name,id)<>(TEST.tupled,TEST.unapply _)
}

object TESTCon{
    val table= new TableQuery(new TestTable(_))
    def getByID(id:Long)={
        val user=table.filter(_.id===id).result.headOption
        Global.db.run(user)
    }
    def find(id:Long)={
        val user=table.filter(_.id===id).result.headOption
//
        Global.db.run(user)
    }
    def insert(testuser:TEST)={
       val result= Global.db.run((table returning table.map(_.id)) +=testuser).map(newId=>testuser.copy(id=newId))
       result
    }
    def delete(id:Long)={
        // Global.db.run(table.filter(_.id===id).delete)
        find(id).flatMap{
           case  Some(testuser) =>
           val delete=table.filter(_.id===id).delete
           Global.db.run(delete).map(_ =>Some(testuser))
           case None =>
           Future.failed(new Exception("User not found"))
        }
    }

    def findAll()={

        val s = Global.db.stream(table.result)


        Global.db.run(table.result)
    }

    def stream() = {
      val s = Global.db.stream(table.result)


    }
}

object TestActor {
    case class Find(id:Long)
    case class Insert(testuser:TEST)
    case class Delete(id:Long)
    case class FindAll()
}
import models.TestActor.{Find,Insert,Delete,FindAll}

class TestActor() extends Actor{
     override def receive:Receive={
        case Find(id:Long) => TESTCon.find(id) pipeTo sender
        case Insert(testuser:TEST) =>TESTCon.insert(testuser) pipeTo sender
        case Delete(id:Long) =>TESTCon.delete(id) pipeTo sender
        case FindAll() => TESTCon.findAll() pipeTo sender
    }

}



