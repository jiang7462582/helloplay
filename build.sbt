name := """helloplay"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.10.5"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  specs2 % Test,
  filters,
  evolutions,
 "mysql" % "mysql-connector-java" % "5.1.27",
 "com.typesafe.akka" % "akka-actor_2.10" % "2.3.5"
)

libraryDependencies += "com.typesafe.slick" % "slick_2.10" % "3.0.0"


resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

// move out API Doc after dist
sources in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false


