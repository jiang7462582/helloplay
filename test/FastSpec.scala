/**
 * Created by jiang on 15-9-23.
 */

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._

import play.api.libs.json._

@RunWith(classOf[JUnitRunner])
class FastSpec extends Specification {

  "The fastSpec Test" should{
    "Query shoud be OK" in new WithApplication {
      val home = route(FakeRequest(GET,"/user/all")).get
      status(home) must equalTo(OK)
      contentType(home) must beSome.which(_ =="application/json")
    }

    "Insert data shoud be OK" in new WithApplication {
     val json:JsValue = Json.parse("""{"name":"jiang"}""")
      val request = FakeRequest(POST,"/user").withJsonBody(json)
      val home = route(request).get
      status(home) must equalTo(200)
      contentType(home) must beSome.which(_ =="application/json")
      contentAsString(home) must contain("jiang")
    }

  }



}
